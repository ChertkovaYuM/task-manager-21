package ru.tsc.chertkova.tm.command;

import ru.tsc.chertkova.tm.api.model.ICommand;
import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IServiceLocator;
import ru.tsc.chertkova.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    protected IServiceLocator serviceLocator;

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    public void showError(final String param) {
        System.out.printf("Error! This argument '%s' not supported...\n", param);
    }

}
