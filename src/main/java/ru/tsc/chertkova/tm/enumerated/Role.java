package ru.tsc.chertkova.tm.enumerated;

public enum Role {

    USUAL("Usual user"),

    ADMIN("Administrator"),

    TEST("Test");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
